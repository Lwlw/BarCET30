﻿namespace BarCET30
{
    public partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.labelOut = new System.Windows.Forms.Label();
            this.labelPerc = new System.Windows.Forms.Label();
            this.buttonCria = new System.Windows.Forms.Button();
            this.comboBoxMenu = new System.Windows.Forms.ComboBox();
            this.pBox = new System.Windows.Forms.PictureBox();
            this.pictureBox2 = new System.Windows.Forms.PictureBox();
            this.tBarTorneira = new System.Windows.Forms.TrackBar();
            this.label2 = new System.Windows.Forms.Label();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.labelTitulo = new System.Windows.Forms.Label();
            this.labelQtddDesej = new System.Windows.Forms.Label();
            this.upDownQtdd = new System.Windows.Forms.NumericUpDown();
            this.buttonEnche = new System.Windows.Forms.Button();
            this.buttonEsvazia = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.pBox)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tBarTorneira)).BeginInit();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.upDownQtdd)).BeginInit();
            this.SuspendLayout();
            // 
            // labelOut
            // 
            this.labelOut.AutoSize = true;
            this.labelOut.Font = new System.Drawing.Font("Segoe Print", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelOut.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.labelOut.Location = new System.Drawing.Point(265, 248);
            this.labelOut.Margin = new System.Windows.Forms.Padding(0);
            this.labelOut.Name = "labelOut";
            this.labelOut.Size = new System.Drawing.Size(0, 19);
            this.labelOut.TabIndex = 0;
            // 
            // labelPerc
            // 
            this.labelPerc.AutoSize = true;
            this.labelPerc.BackColor = System.Drawing.Color.Black;
            this.labelPerc.Font = new System.Drawing.Font("Segoe Print", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelPerc.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.labelPerc.Location = new System.Drawing.Point(214, 30);
            this.labelPerc.Name = "labelPerc";
            this.labelPerc.Size = new System.Drawing.Size(0, 23);
            this.labelPerc.TabIndex = 2;
            // 
            // buttonCria
            // 
            this.buttonCria.Font = new System.Drawing.Font("Segoe Print", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonCria.ForeColor = System.Drawing.SystemColors.Highlight;
            this.buttonCria.Location = new System.Drawing.Point(50, 104);
            this.buttonCria.Name = "buttonCria";
            this.buttonCria.Size = new System.Drawing.Size(138, 28);
            this.buttonCria.TabIndex = 3;
            this.buttonCria.Text = "~   Novo copo   ~";
            this.buttonCria.UseVisualStyleBackColor = true;
            this.buttonCria.Click += new System.EventHandler(this.buttonCria_Click);
            // 
            // comboBoxMenu
            // 
            this.comboBoxMenu.BackColor = System.Drawing.Color.DimGray;
            this.comboBoxMenu.Font = new System.Drawing.Font("Segoe Print", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.comboBoxMenu.ForeColor = System.Drawing.SystemColors.Menu;
            this.comboBoxMenu.FormattingEnabled = true;
            this.comboBoxMenu.Location = new System.Drawing.Point(50, 14);
            this.comboBoxMenu.Name = "comboBoxMenu";
            this.comboBoxMenu.Size = new System.Drawing.Size(138, 27);
            this.comboBoxMenu.TabIndex = 9;
            this.comboBoxMenu.Text = "- selecionar bebida -";
            this.comboBoxMenu.SelectedIndexChanged += new System.EventHandler(this.comboBoxMenu_SelectedIndexChanged);
            // 
            // pBox
            // 
            this.pBox.BackColor = System.Drawing.Color.Transparent;
            this.pBox.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.pBox.Location = new System.Drawing.Point(223, 66);
            this.pBox.Name = "pBox";
            this.pBox.Size = new System.Drawing.Size(170, 170);
            this.pBox.TabIndex = 8;
            this.pBox.TabStop = false;
            // 
            // pictureBox2
            // 
            this.pictureBox2.BackColor = System.Drawing.Color.Transparent;
            this.pictureBox2.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.pictureBox2.Location = new System.Drawing.Point(222, 268);
            this.pictureBox2.Name = "pictureBox2";
            this.pictureBox2.Size = new System.Drawing.Size(170, 170);
            this.pictureBox2.TabIndex = 6;
            this.pictureBox2.TabStop = false;
            // 
            // tBarTorneira
            // 
            this.tBarTorneira.Location = new System.Drawing.Point(6, 22);
            this.tBarTorneira.Maximum = 2;
            this.tBarTorneira.Minimum = -2;
            this.tBarTorneira.Name = "tBarTorneira";
            this.tBarTorneira.Size = new System.Drawing.Size(126, 45);
            this.tBarTorneira.TabIndex = 10;
            this.tBarTorneira.Scroll += new System.EventHandler(this.tBarTorneira_Scroll);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.BackColor = System.Drawing.Color.Transparent;
            this.label2.Font = new System.Drawing.Font("Segoe Print", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.label2.Location = new System.Drawing.Point(11, 46);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(118, 19);
            this.label2.TabIndex = 12;
            this.label2.Text = "-           0           +";
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.tBarTorneira);
            this.groupBox1.Font = new System.Drawing.Font("Segoe Print", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox1.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.groupBox1.Location = new System.Drawing.Point(51, 165);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(138, 72);
            this.groupBox1.TabIndex = 13;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "<Torneira>";
            // 
            // labelTitulo
            // 
            this.labelTitulo.AutoSize = true;
            this.labelTitulo.BackColor = System.Drawing.Color.Gainsboro;
            this.labelTitulo.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.labelTitulo.Cursor = System.Windows.Forms.Cursors.Default;
            this.labelTitulo.Font = new System.Drawing.Font("Segoe Print", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelTitulo.ForeColor = System.Drawing.SystemColors.Highlight;
            this.labelTitulo.Location = new System.Drawing.Point(223, 14);
            this.labelTitulo.Name = "labelTitulo";
            this.labelTitulo.Size = new System.Drawing.Size(168, 30);
            this.labelTitulo.TabIndex = 14;
            this.labelTitulo.Text = "~   Afunda Bar   ~";
            // 
            // labelQtddDesej
            // 
            this.labelQtddDesej.AutoSize = true;
            this.labelQtddDesej.BackColor = System.Drawing.Color.Transparent;
            this.labelQtddDesej.Font = new System.Drawing.Font("Segoe Print", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelQtddDesej.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.labelQtddDesej.Location = new System.Drawing.Point(47, 46);
            this.labelQtddDesej.Name = "labelQtddDesej";
            this.labelQtddDesej.Size = new System.Drawing.Size(147, 19);
            this.labelQtddDesej.TabIndex = 16;
            this.labelQtddDesej.Text = "Quantidade desejada (cl):";
            // 
            // upDownQtdd
            // 
            this.upDownQtdd.Font = new System.Drawing.Font("Segoe Print", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.upDownQtdd.Location = new System.Drawing.Point(50, 66);
            this.upDownQtdd.Name = "upDownQtdd";
            this.upDownQtdd.Size = new System.Drawing.Size(138, 27);
            this.upDownQtdd.TabIndex = 17;
            this.upDownQtdd.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // buttonEnche
            // 
            this.buttonEnche.Font = new System.Drawing.Font("Segoe Print", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonEnche.ForeColor = System.Drawing.SystemColors.Highlight;
            this.buttonEnche.Location = new System.Drawing.Point(50, 135);
            this.buttonEnche.Name = "buttonEnche";
            this.buttonEnche.Size = new System.Drawing.Size(69, 23);
            this.buttonEnche.TabIndex = 18;
            this.buttonEnche.Text = "Encher";
            this.buttonEnche.UseVisualStyleBackColor = true;
            this.buttonEnche.Click += new System.EventHandler(this.buttonEnche_Click);
            // 
            // buttonEsvazia
            // 
            this.buttonEsvazia.Font = new System.Drawing.Font("Segoe Print", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonEsvazia.ForeColor = System.Drawing.SystemColors.Highlight;
            this.buttonEsvazia.Location = new System.Drawing.Point(120, 136);
            this.buttonEsvazia.Name = "buttonEsvazia";
            this.buttonEsvazia.Size = new System.Drawing.Size(69, 23);
            this.buttonEsvazia.TabIndex = 19;
            this.buttonEsvazia.Text = "Esvaziar";
            this.buttonEsvazia.UseVisualStyleBackColor = true;
            this.buttonEsvazia.Click += new System.EventHandler(this.buttonEsvazia_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.DimGray;
            this.ClientSize = new System.Drawing.Size(443, 284);
            this.Controls.Add(this.buttonEsvazia);
            this.Controls.Add(this.buttonEnche);
            this.Controls.Add(this.upDownQtdd);
            this.Controls.Add(this.labelQtddDesej);
            this.Controls.Add(this.labelTitulo);
            this.Controls.Add(this.labelPerc);
            this.Controls.Add(this.comboBoxMenu);
            this.Controls.Add(this.pictureBox2);
            this.Controls.Add(this.buttonCria);
            this.Controls.Add(this.labelOut);
            this.Controls.Add(this.pBox);
            this.Controls.Add(this.groupBox1);
            this.Name = "Form1";
            this.Text = "Form1";
            this.Load += new System.EventHandler(this.Form1_Load);
            ((System.ComponentModel.ISupportInitialize)(this.pBox)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tBarTorneira)).EndInit();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.upDownQtdd)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label labelOut;
        private System.Windows.Forms.Label labelPerc;
        private System.Windows.Forms.Button buttonCria;
        private System.Windows.Forms.PictureBox pictureBox2;
        private System.Windows.Forms.PictureBox pBox;
        private System.Windows.Forms.ComboBox comboBoxMenu;
        private System.Windows.Forms.TrackBar tBarTorneira;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Label labelTitulo;
        private System.Windows.Forms.Label labelQtddDesej;
        private System.Windows.Forms.NumericUpDown upDownQtdd;
        private System.Windows.Forms.Button buttonEnche;
        private System.Windows.Forms.Button buttonEsvazia;
    }
}

