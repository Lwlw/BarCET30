﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;


namespace BarCET30
{
    public partial class Form1 : Form
    {

        public void encheAnim()
        {
            if (labelOut.Text != "A encher......" && labelOut.Text != "A esvaziar......")
                labelOut.Text += ".";
            else if (labelOut.Text == "A encher......")
                labelOut.Text = "A encher";
            else if (labelOut.Text == "A esvaziar......")
                labelOut.Text = "A esvaziar";
        }


        private Copo meuCopo;

        private string[,] _bebidas;

        Bitmap bmp;
        Graphics g;
        Timer t = new Timer();
        double pbUnidad;
        int pbWidth, pbHeight, pbComplet;
        bool buttonCall;
        bool _encher;

        public Form1()
        {
            InitializeComponent();

            pictureBox2.BackColor = Color.Transparent;
            pictureBox2.Parent = pBox;
            pictureBox2.Location = new Point(0, 0);
            //pictureBox2.Image = Image.FromFile("../../Resources/redWine.png");
            pictureBox2.SizeMode = PictureBoxSizeMode.StretchImage;

        }


        private void Form1_Load(object sender, EventArgs e)
        {
            //Lê o conteudo do ficheiro e carrega a info no array _bebida
            //A partir daí as bebidas são adicionadas à comboBox e a info é processada para a matriz _bebidas
            
            string[] linhasBebidas = File.ReadAllLines("myDrinks.txt");
            Array.Sort(linhasBebidas);

            _bebidas = new string[linhasBebidas.Length, 3];


            for (int i = 0; i < linhasBebidas.Length; i++)
            {
                string[] _bebida = linhasBebidas[i].Split(',');
                comboBoxMenu.Items.Add(_bebida[0]);

                _bebidas[i, 0] = _bebida[0];
                _bebidas[i, 1] = _bebida[1];
                _bebidas[i, 2] = _bebida[2];
            }


            //Botões e outros elementos são desativados
            //apenas sendo possível interagir através da combo box, inicialmente

            comboBoxMenu.SelectedIndex = 0;

            upDownQtdd.Enabled = false;
            buttonCria.Enabled = false;
            buttonEnche.Enabled = false;
            buttonEsvazia.Enabled = false;
            groupBox1.Enabled = false;
            

            //Inicialização de variáveis ligadas ao grafismo e animação; 
            //criação de event handler associado ao objecto da classe Timer que possibilita a 
            //animação do enchimento do copo

            pbWidth = pBox.Width;                       
            pbHeight = pBox.Height;

            pbUnidad = pbHeight / 100.0;

            bmp = new Bitmap(pbWidth, pbHeight);

            t.Tick += new EventHandler(this.t_Tick);

            labelPerc.Hide();

        }


        // Simula o enchimento do copo em 'modo torneira'
        // posição da track bar determina a ação:
        // 0 parar; 1,2/-1,-2 velocidades de enchimento/esvaziamento

        private void tBarTorneira_Scroll(object sender, EventArgs e)
        {
            labelOut.Location = new Point(260, labelOut.Top);

            buttonCall = false;

            if (tBarTorneira.Value > 0 && pbComplet > 0)
            {
                if (_encher) pbComplet++; else pbComplet--;
                _encher = true;
                meuCopo.Contem = meuCopo.Capacidade;
                labelOut.Text = "A encher";
                t.Start();
            }

            else if (tBarTorneira.Value < 0 && pbComplet < 100)
            {
                if (_encher) pbComplet++; else pbComplet--;
                _encher = false;
                meuCopo.Contem = meuCopo.Capacidade;
                labelOut.Text = "A esvaziar";
                t.Start();
            }

            else
            {
                t.Stop();
                meuCopo.Contem = Convert.ToInt32(((double)(100 - pbComplet) / 100.0) * meuCopo.Capacidade);
                labelOut.Text = meuCopo.ToString();
                labelOut.Location = new Point(this.Width / 2 - labelOut.Width / 2, labelOut.Top);
            }

        }


        // Botão que cria uma instancia da Classe Copo
        // com os atributos selecionados pelo utilizador
        // Iniciado o timer pelo método Start() que despoleta a anim do gráfico, até à percentagem 
        // correspondente à quantidade designada, pelo ratio liquido contido/capacidade do copo

        private void buttonCria_Click(object sender, EventArgs e)
        {
            if (comboBoxMenu.SelectedIndex == 0)
                return;

            labelOut.Location = new Point(260, labelOut.Top);

            meuCopo = new Copo(_bebidas[comboBoxMenu.SelectedIndex, 0], Convert.ToInt32(_bebidas[comboBoxMenu.SelectedIndex, 1]));

            pictureBox2.Image = Image.FromFile("../../Resources/" + _bebidas[comboBoxMenu.SelectedIndex, 2]);

            groupBox1.Enabled = true;
            buttonEnche.Enabled = true;
            buttonEsvazia.Enabled = true;

            pbComplet = 100;
            t.Interval = 100;

            _encher = true;
            meuCopo.Encher((double)upDownQtdd.Value);
            buttonCall = true;
            labelOut.Text = "A encher";

            t.Start();
        }

        //botoes de enchimento/esvaziamento
        // É feito uso dos respectivos métodos da classe Copo

        private void buttonEnche_Click(object sender, EventArgs e)
        {
            if (!t.Enabled && meuCopo.ValorEmPercentagem() < 100)
            {
                if (_encher) pbComplet++; else pbComplet--;
                _encher = true;
                labelOut.Location = new Point(260, labelOut.Top);
                meuCopo.Encher((double)upDownQtdd.Value);
                buttonCall = true;
                labelOut.Text = "A encher";
                t.Start();
            }
        }
        private void buttonEsvazia_Click(object sender, EventArgs e)
        {
            if (!t.Enabled && meuCopo.ValorEmPercentagem() > 0)
            {
                if (_encher) pbComplet++; else pbComplet--;
                _encher = false;
                labelOut.Location = new Point(260, labelOut.Top);
                meuCopo.Esvaziar((double)upDownQtdd.Value);
                buttonCall = true;
                labelOut.Text = "A esvaziar";
                t.Start();
            }
        }


        private void comboBoxMenu_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (comboBoxMenu.SelectedIndex != 0)
            {
                buttonCria.Enabled = true;
                upDownQtdd.Enabled = true;
            }
            else
            {
                buttonCria.Enabled = false;
                upDownQtdd.Enabled = false;
            }
        }


        // método chamado no intervalo especificado (classe Timer) 
        // onde é realizada a animação (classe Graphics, método fillRectangle) 
        // e lançado o output através da manipulação de variáveis e da utilização
        // dos novos valores e propriedades alteradas pelo user, 

        private void t_Tick(object sender, EventArgs e)
        {

            g = Graphics.FromImage(bmp);
            g.Clear(Color.DodgerBlue);

            g.FillRectangle(Brushes.DimGray, new Rectangle(0, 0, pbWidth, (int)(pbComplet * pbUnidad)));

            labelPerc.Text = Convert.ToString(100 - pbComplet) + "%";
            g.DrawString(Convert.ToString(100 - pbComplet) + "%", new Font("Segoe Print", 10, FontStyle.Bold), Brushes.White, new PointF(pBox.Width / 2 - labelPerc.Width / 2, 5));

            pBox.Image = bmp;

            if (buttonCall && _encher)
                pbComplet--;
            else if(buttonCall && !_encher)
                pbComplet++;
            else
            {
                if (pbComplet < 5 || pbComplet > 95)
                    pbComplet = tBarTorneira.Value > 0 ? pbComplet - 1 : pbComplet + 1;
                else
                {
                    pbComplet = -1 * tBarTorneira.Value + pbComplet;
                    if (!(pbComplet % 2 == 0))
                    {
                        encheAnim();
                        goto impar;
                    }

                }
            }

            if (pbComplet % 2 == 0)
                encheAnim();

            impar:

            if (!buttonCall && (pbComplet > 100 || pbComplet < 0))
            {
                if (pbComplet < 0) pbComplet = 0;
                if (pbComplet > 100) pbComplet = 100;
                g.Dispose();
                meuCopo.Contem = Convert.ToInt32(((double)(100 - pbComplet) / 100.0) * meuCopo.Capacidade);
                labelOut.Text = meuCopo.ToString();
                labelOut.Location = new Point(this.Width / 2 - labelOut.Width / 2, labelOut.Top);
                tBarTorneira.Value = 0;
                t.Stop();
            }

            if (buttonCall && (_encher ? pbComplet < 100 - meuCopo.ValorEmPercentagem() : 
                pbComplet > 100 - meuCopo.ValorEmPercentagem()))
            {
                g.Dispose();
                labelOut.Text = meuCopo.ToString();
                labelOut.Location = new Point(this.Width / 2 - labelOut.Width / 2, labelOut.Top);
                t.Stop();
            }

        }
    }
}
